package com.demo.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ConversionUtilsTest {

	static ConversionUtils  utils;
	
	@BeforeAll	
	public static void setUpBeforeAll() {
		utils =  new ConversionUtils();
	}
	
	
	@Test
	public void convertToDecimalTest1() {
		assertThrows(ArithmeticException.class, () -> utils.convertToDecimal(3, 0));
	}
	
	@Test
	public void convertToDecimalTest2() {
		assertEquals(0.75, utils.convertToDecimal(3, 4));
	}
	
	@AfterAll
	public static void tearDownAfterAll() {
		utils = null;
	}

}
