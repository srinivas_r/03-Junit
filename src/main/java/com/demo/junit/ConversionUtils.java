package com.demo.junit;

public class ConversionUtils {
	
	public double convertToDecimal(int numerator, int denominator) throws ArithmeticException {
		if( denominator == 0) {
			throw  new ArithmeticException();
		}
		return  (double)numerator / (double)denominator;
	}

}
